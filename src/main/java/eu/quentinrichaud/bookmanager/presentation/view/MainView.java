package eu.quentinrichaud.bookmanager.presentation.view;

import eu.quentinrichaud.bookmanager.domain.Author;
import eu.quentinrichaud.bookmanager.domain.Book;
import eu.quentinrichaud.bookmanager.presentation.model.EditorViewData;
import eu.quentinrichaud.bookmanager.presentation.model.MainViewData;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

import java.util.ArrayList;
import java.util.List;


public class MainView extends Application {

    @Override
    public void start(Stage primaryStage) throws Exception {
        /* Store all screens in the ScreenHelper */
        Parent indexScreen = FXMLLoader.load(getClass().getResource("/fxml/MainView.fxml"));
        Parent editionScreen = FXMLLoader.load(getClass().getResource("/fxml/EditorView.fxml"));
        ScreenHelper screenHelper = ScreenHelper.getSingleton();
        screenHelper.addScreen("index", indexScreen);
        screenHelper.addScreen("edition", editionScreen);

        Scene mainScene = new Scene(indexScreen, 300, 275);
        screenHelper.setMainScene(mainScene);

        primaryStage.setTitle("Book Manager");
        primaryStage.setScene(mainScene);
        primaryStage.setMaximized(true);
        primaryStage.show();
    }

    public static void main(String[] args) {
        mockData();

        launch(args);
    }


    /**
     * This should be deleted once we implement correctly the business layer
     * and the data layer of our application
     */
    protected static void mockData() {
        /**
         * Generate Authors instances
         */
        List<Author> authors = new ArrayList<Author>();
        authors.add(new Author(0, "Michel Houellebecq"));
        authors.add(new Author(1, "Cécile Coulon"));
        authors.add(new Author(2, "Leonardo Padura"));

        /**
         * Put fake data in the MainViewData singleton
         */
        MainViewData mainViewData = MainViewData.getSingleton();

        /**
         * Generate books
         */
        List<Book> initialBooks = new ArrayList<Book>();
        initialBooks.add(
                new Book(
                        0,
                        "Extension du domaine de la lutte",
                        "Cadre moyen, analyste-programmeur dans une société informatique, son salaire net atteint 2,5 fois le SMIC. Malgré cette insertion sociale, il n’attire pas les femmes. Dépourvu de beauté comme de charme, sujet à de fréquents accès dépressifs",
                        authors.get(0)
                )
        );
        initialBooks.add(
                new Book(
                        1,
                        "Les Particules élémentaires",
                        "Leur mère, Janine, a vécu à fond les idéaux de la société permissive. Née en 1928, elle grandit en Algérie (où son père était venu travailler comme ingénieur) et vint à Paris pour compléter ses études",
                        authors.get(0)
                )
        );
        initialBooks.add(
                new Book(
                        2,
                        "Méfiez-vous des enfants sages",
                        "Parallèlement, elle participe aux côtés de Marie Darrieussecq, Marion Aubert et Adélaïde de Clermont-Tonnerre, à la septième édition du Paris des Femmes",
                        authors.get(1)
                )
        );
        initialBooks.add(
                new Book(
                        3,
                        "Une bête au paradis",
                        "Cécile Coulon continue avec ce septième livre a explorer un « univers bien à elle dans lequel les âmes désespérées connaissent des soubresauts vers la vie",
                        authors.get(1)
                )
        );
        initialBooks.add(
                new Book(
                        4,
                        "Pasado perfecto",
                        "Dans L'Automne à Cuba (1998), Mario Conde démissionne de la police et mène une enquête littéraire dans Adiós Hemingway (2001)",
                        authors.get(2)
                )
        );
        initialBooks.add(
                new Book(
                        5,
                        "La cola de la serpiente",
                        "Mario Conde, célibataire, d'abord au milieu de la trentaine dans les premiers romans, puis quadragénaire, évolue donc dans des récits subtilement agencés",
                        authors.get(2)
                )
        );

        /** Insert the created books into mainViewData */
        mainViewData.updateBookList(initialBooks);


        /** Insert the authors into editorViewData */
        EditorViewData editorViewData = EditorViewData.getSingleton();
        editorViewData.updateAuthorList(authors);
    }
}