package eu.quentinrichaud.bookmanager.presentation.model;

import eu.quentinrichaud.bookmanager.domain.Author;
import eu.quentinrichaud.bookmanager.domain.Book;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

import java.util.Collection;

/**
 * This class stores all the data needed by the JavaFX view EditorView
 *
 */
public class EditorViewData {
    private static EditorViewData singleton = new EditorViewData();

    /**
     * The list of all authors. This is needed for the ComboBox in which
     * the authors are suggested.
     */
    private ObservableList<Author> authorList = FXCollections.observableArrayList();

    /**
     *  This is the book instance that is being edited in the EditorView.
     *  It is wrapped in an ObjectProperty, that is need by JavaFX in order to know when the
     *  selected book changes.
     */
    private ObjectProperty<Book> currentlyEditedBookProperty = new SimpleObjectProperty<>(null);

    /**
     * Get the Book object currently being edited
     * @return
     */
    public Book getCurrentlyEditedBook() {
        return this.currentlyEditedBookProperty.get();
    }

    /**
     * Get the ObjectProperty wrapping the Book object currently being edited
     * @return
     */
    public ObjectProperty<Book> getCurrentlyEditedBookProperty() {
        return this.currentlyEditedBookProperty;
    }

    /**
     * Update the book currently edited. It changes the Book instance contained into
     * the ObjectProperty object of the attribute this.currentlyEditedBook
     * @param book The new book instance to put as currently edited book
     */
    public void setCurrentlyEditedBook(Book book) {
        this.currentlyEditedBookProperty.set(book);
    }


    /**
     * This class works as a singleton, that means there one single instance
     * for the whole application.
     *
     * This method returns the single instance (the singleton)
     *
     * @return
     */
    public static EditorViewData getSingleton() {
        return singleton;
    }

    /**
     * Getter for the list of authors
     * @return
     */
    public ObservableList<Author> getAuthorList() {
        return this.authorList;
    }

    /**
     * Update the list of authors to display in the comboBox
     *
     * @param authors The new collection of authors to display
     */
    public void updateAuthorList(Collection<Author> authors) {
        this.authorList.setAll(authors);
    }
}
