package eu.quentinrichaud.bookmanager.presentation.model;

import com.sun.tools.javac.Main;
import eu.quentinrichaud.bookmanager.domain.Book;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

import java.util.Collection;

/**
 * This class stores the data needed by the JavaFX view MainView
 */
public class MainViewData {
    private static MainViewData singleton = new MainViewData();

    /**
     * The list of books to display in the index screen. This is the result
     * of the search performed by the user, or all the existing books if
     * the user made no search
     *
     */
    private ObservableList<Book> bookList = FXCollections.observableArrayList();


    /**
     * Update the list of books to display in the index screen
     *
     * @param newBookList The new collection of books to display
     */
    public void updateBookList(Collection<Book> newBookList) {
        this.bookList.setAll(newBookList);
    }

    /**
     * Getter for the bookList attribute.
     * @return
     */
    public ObservableList<Book> getBookList() {
        return bookList;
    }


    /**
     * This class works as a singleton, that means there one single instance
     * for the whole application.
     *
     * This method returns the single instance (the singleton)
     *
     * @return
     */
    public static MainViewData getSingleton() {
        return singleton;
    }



}
