package eu.quentinrichaud.bookmanager.presentation.controllers;

import com.sun.tools.javac.Main;
import eu.quentinrichaud.bookmanager.domain.Book;
import eu.quentinrichaud.bookmanager.presentation.model.EditorViewData;
import eu.quentinrichaud.bookmanager.presentation.model.MainViewData;
import eu.quentinrichaud.bookmanager.presentation.view.MainView;
import eu.quentinrichaud.bookmanager.presentation.view.ScreenHelper;
import javafx.fxml.FXML;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.scene.input.MouseEvent;

public class MainViewController {

    @FXML
    private ListView<Book> bookIndexList;

    @FXML
    private void initialize() {
        MainViewData mainViewData = MainViewData.getSingleton();

        /** bookIndexList initialization */
        /** 1. Bind data, get the list of books stored in an observable collection
         * in our data store singleton
         */
        bookIndexList.setItems(mainViewData.getBookList());
        /**
         * 2. Setup the text to be displayed in each list cell (the ListView object
         * cannot guess what to display from the Book object)
         */
        bookIndexList.setCellFactory(listView -> new ListCell<Book>() {
            @Override
            public void updateItem(Book book, boolean empty) {
                super.updateItem(book, empty);
                if (empty) {
                    setText(null);
                } else {
                    setText(book.getTitle());
                }
            }
        });
    }


    public void bookIndexListOnMouseClicked(MouseEvent mouseEvent) {
        /** Get the selected line */
        Book selectedBook = bookIndexList.getSelectionModel().getSelectedItem();
        if (selectedBook != null) {
            EditorViewData editorViewData = EditorViewData.getSingleton();
            editorViewData.setCurrentlyEditedBook(selectedBook);
            ScreenHelper.getSingleton().switchScreen("edition");
        }
    }
}
