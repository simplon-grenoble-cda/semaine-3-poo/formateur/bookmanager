package eu.quentinrichaud.bookmanager.presentation.controllers;

import eu.quentinrichaud.bookmanager.domain.Author;
import eu.quentinrichaud.bookmanager.presentation.model.EditorViewData;
import eu.quentinrichaud.bookmanager.presentation.view.ScreenHelper;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.util.Callback;

public class EditorViewController {
    @FXML
    private ComboBox<Author> authorComboBox;

    @FXML
    private TextField titleTextField;

    @FXML
    private TextArea summaryTextArea;

    Callback<ListView<Author>, ListCell<Author>> authorComboBoxCellFactory = new Callback<ListView<Author>, ListCell<Author>>() {
        @Override
        public ListCell<Author> call(ListView<Author> l) {
            return new ListCell<Author>() {
                @Override
                protected void updateItem(Author author, boolean empty) {
                    super.updateItem(author, empty);
                    if (author == null || empty) {
                        setGraphic(null);
                    } else {
                        setText(author.getName());
                    }
                }
            };
        }
    };

    @FXML
    private void initialize() {
        EditorViewData editorViewData = EditorViewData.getSingleton();

        authorComboBox.setItems(editorViewData.getAuthorList());
        authorComboBox.setCellFactory(authorComboBoxCellFactory);
        authorComboBox.setButtonCell(authorComboBoxCellFactory.call(null));

        /**
         * Here we react to the event of the selected book changing, and we update
         * the content of the fields in the form with the content of the existing book
         */
        editorViewData.getCurrentlyEditedBookProperty().addListener((obs, oldValue, newBook) -> {
            /** Update the title and summary fields */
            this.titleTextField.setText(newBook.getTitle());
            this.summaryTextArea.setText(newBook.getSummary());
            /** Update the comboBox to display the author of the selected book */
            int currentAuthorIndex = editorViewData.getAuthorList().indexOf(newBook.getAuthor());
            authorComboBox.getSelectionModel().select(currentAuthorIndex);
        });
    }

    public void backButtonActionHandler(ActionEvent actionEvent) {
        ScreenHelper.getSingleton().switchScreen("index");
    }

    public void authorComboBoxOnAction(ActionEvent actionEvent) {
        Author selectedAuthor = authorComboBox.getSelectionModel().getSelectedItem();
        System.out.println("Selected author " + selectedAuthor.getName());
    }
}
