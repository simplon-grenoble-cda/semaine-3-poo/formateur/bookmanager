package eu.quentinrichaud.bookmanager.domain;

import java.util.Objects;

public class Book {
    protected long id;
    protected String title;
    protected String summary;
    protected Author author;


    public Book(long id, String title, String summary, Author author) {
        this.id = id;
        this.title = title;
        this.summary = summary;
        this.author = author;
    }

    public String getTitle() {
        return title;
    }

    public String getSummary() {
        return summary;
    }

    public Author getAuthor() {
        return author;
    }

    @Override
    public boolean equals(Object o) {
        // self check
        if (this == o)
            return true;
        // null check
        if (o == null)
            return false;

        // type check and cast
        if (getClass() != o.getClass())
            return false;

        Book book = (Book) o;
        return book.id == this.id;
    }
}
