package eu.quentinrichaud.bookmanager.domain;

public class Author {
    protected String name;
    protected long id;

    public Author(long id, String name) {
        this.id = id;
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public boolean equals(Object o) {
        // self check
        if (this == o)
            return true;
        // null check
        if (o == null)
            return false;

        // type check and cast
        if (getClass() != o.getClass())
            return false;

        Author author = (Author) o;
        return author.id == this.id;
    }
}
