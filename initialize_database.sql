use book_manager;

DROP TABLE IF EXISTS `Books`;
DROP TABLE IF EXISTS `Authors`;

CREATE TABLE Authors (
  id bigint(255) NOT NULL AUTO_INCREMENT, 
  name VARCHAR(999),
  PRIMARY KEY (id)
);

CREATE TABLE Books (
  id bigint(255) NOT NULL AUTO_INCREMENT, 
  title VARCHAR(999), 
  summary MEDIUMTEXT, 
  author_id bigint(255),
  PRIMARY KEY (id),
  FOREIGN KEY (author_id) REFERENCES Authors(id)
);


INSERT INTO Authors (id, name)
VALUES
    (1, 'Michel Houellebecq'),
    (2, 'Cécile Coulon'),
    (3, 'Leonardo Padura');

INSERT INTO Books(title, summary, author_id)
VALUES
    ('Extension du domaine de la lutte', 'Cadre moyen, analyste-programmeur dans une société informatique, son salaire net atteint 2,5 fois le SMIC. Malgré cette insertion sociale, il n’attire pas les femmes. Dépourvu de beauté comme de charme, sujet à de fréquents accès dépressifs', 1),
    ('Les Particules élémentaires', 'Leur mère, Janine, a vécu à fond les idéaux de la société permissive. Née en 1928, elle grandit en Algérie (où son père était venu travailler comme ingénieur) et vint à Paris pour compléter ses études', 1),
    ('Méfiez-vous des enfants sages', 'Parallèlement, elle participe aux côtés de Marie Darrieussecq, Marion Aubert et Adélaïde de Clermont-Tonnerre, à la septième édition du Paris des Femmes', 2),
    ('Une bête au paradis', 'Cécile Coulon continue avec ce septième livre a explorer un « univers bien à elle dans lequel les âmes désespérées connaissent des soubresauts vers la vie', 2),
    ('Pasado perfecto', 'Dans L\'Automne à Cuba (1998), Mario Conde démissionne de la police et mène une enquête littéraire dans Adiós Hemingway (2001)', 3),
    ('La cola de la serpiente', 'Mario Conde, célibataire, d\'abord au milieu de la trentaine dans les premiers romans, puis quadragénaire, évolue donc dans des récits subtilement agencés', 3);
