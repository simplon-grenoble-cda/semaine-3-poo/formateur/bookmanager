Instructions de l'execice ici : https://docs.google.com/document/d/1kTzzcC4J75TOI-nLworr175_1WCWo9waVo1j2dRVb8Y/edit?usp=sharing


# Instructions pour lancer le projet

## 1. Mettre en place la base de données

Il faut avoir un serveur de base de données qui tourne sur votre machine. Deux options pour celà : 

1. Installer localement MySQL server (chercher sur internet les instructions pour une installation sur Ubuntu)
2. Faire tourne MySQL server sur un container Docker (virtualisation)

### 1. Installation locale de MySQL Server

Des veilles ont été faites sur le sujet : https://drive.google.com/drive/folders/11dywbwkqK2EVV7091C8mfnfG5QtNPbjR

Pour être sur que le serveur de base de données tourne correctement, vous devriez pouvoir vous y connecter en ligne de commande
(avec le client `mysql`), avec l'utilisateur et le mot de passe par défaut configuré pour le serveur.

### 2. MySQL server sur un container docker

Un repo de demo existe ici : https://gitlab.com/simplon-grenoble-cda/semaine-3-poo/formateur/mysql-docker-demo

Si vous arrivez à suivre les instructions pour installer docker & docker-compose sur votre machine, vous devriez pouvoir
lancer un serveur MySQL préconfiguré en lançant `docker-compose up` depuis le dossier racine de notre repo. 

### 3. Se connecter au serveur de base de données avec un coupe user/password

Quelque soit la méthode utilisée pour faire tourner le serveur de base de données, on devrait pouvoir s'y connecter avec
la commande : 

```
mysql --user user --password="pass" --host 127.0.0.1 
```

Attention : les valeurs `user` et `pass` dépendent de la façon dont vous avez configuré le serveur. Si vous avez 
utilisé la méthode via docker, ce sont les valeurs configurées par défaut. Si vous avez installé le serveur MySQL
localement sur votre machine, c'est à vous de savoir quel utilisateur/mot de passe vous avez configuré.


### 4. Initialiser la base de données et les tables

Le programme BookManager présuppose l'existence d'une base de données `book_manager` et de ses tables déjà configurées.

Si vous avez installé MySQL localement, créez la base de données `book_manager`. Si vous avez utilisé `docker-compose`,
la base de données a été créée automatiquement.

Lancer le script `./reset_database.sh` permet de faire cette configuration. Attention : dans le script, veillez à changer
les bonnes valeurs pour `user` et `password` si besoin.

### 5. Configurer les infos de connexion à la base de données dans le code Java

(Ce code n'est pas présent dès `master`, mais dans les branches où la couche data est implémentée).

Dans la classe `ConnectionFactory`, modifier les attributs `URL`, `USER` et `PASS` avec les valeurs qui correspondent
à la configuration de votre serveur de base de données.

Note : dans un projet plus avancé, on mettrait ces valeurs dans un fichier `application.properties` ce qui permet de changer
les valeurs sans avoir à éditer le code Java.

## 2. Lancer le projet avec Maven

Le projet BookManager utilise la libraire JavaFX, qui est une dépendence compliquée à configurer manuellement. C'est pourquoi
on a utilisé l'outil de gestion de dépendences Maven pour gérer la dépendence JavaFX.

Pour lancer le projet : 

### Sans IDE 

Il faut installer maven sur votre machine, puis dans le dossier racine du repo, lancer la commande  :

```
mvn javafx:run
```


### Avec IntelliJ

IntelliJ embarque Maven par défaut, pas d'installation supplémentaire requise. Une fois le projet chargé avec IntelliJ, vous
pouvez ouvrir l'onglet "Maven" sur la droite de l'IDE, et cliquer sur la commande "Plugins > javafx > javafx:run". 

Pour plus de détails : voir dans le drive des apprenants, il y a une veille techno sur l'utilisation de Maven.
